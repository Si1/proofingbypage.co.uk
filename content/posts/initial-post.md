---
title: "Initial Post"
date: 2020-11-15T17:00:17Z
draft: false
---

# Heading 1

- Thing 1
- Thing 2
- Thing 3
  
## Heading 2

| Name | Type | Amount |
| --- | --- | --- |
| Cod | Animal | 2 |
| Cat | Animal | 3 |
| Corn | Vegetable | 10 |
| Coal | Mineral | 20 |

### Heading 3

```bash
export FOO="bar"
ls -la
```

#### Heading 4

Some random text

##### Heading 5
